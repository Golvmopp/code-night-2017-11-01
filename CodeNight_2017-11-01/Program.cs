﻿using System;
using System.Collections.Generic;

namespace CodeNight_20171101
{
    class MainClass
    {
        //List<ShoppingItem> Storage = new List<ShoppingItem>();
		//List<Item> ShoppingVart = new List<Item>();
        Dictionary<ShoppingItem, int> Storage = new Dictionary<ShoppingItem, int>();
        Dictionary<ShoppingItem, int> ShoppingCart = new Dictionary<ShoppingItem, int>();
        bool isReturningCustomer = false;
        bool isDone = false;

        public static void Main(string[] args)
        {
            MainClass main = new MainClass();
            main.FillStorage();
            main.CheckIfReturningCustomer();

            while(true)
            {
				main.ListItems();
                main.BuyItem();
                main.ListCart();
                main.CalculateTotalPrice();
            }

        }

        private void ListItems()
        {
            Console.WriteLine("\nHere is what we have in the store...");
            foreach(var item in Storage)
            {
                Console.WriteLine(item.Key.name + " || " + item.Key.price + "kr" + " || " + item.Value + "st" + " || " + "Utgångsdatum:" + item.Key.expireDate);
            }
        }

        private void CheckIfReturningCustomer()
        {
            Console.WriteLine("Hello and welcome to my lovely store. Are you a returning customer? (Y/N)");
            bool didTypeCorrect = false;

            while (!didTypeCorrect)
            {
                string answer = Console.ReadLine();
                answer = answer.ToLower();
                if (answer.Equals("y"))
                {
                    isReturningCustomer = true;
                    didTypeCorrect = true;
                }
                else if (answer.Equals("n"))
                {
                    isReturningCustomer = false;
                    didTypeCorrect = true;
                }
                if (!didTypeCorrect)
                {
                    Console.WriteLine("That is not a correct answer. Try again by typing 'Y' or 'N'");
                }
            }
        }

        private void BuyItem() //Todo: Gör så att personen kan köpa flera exemplar
        {
            Console.WriteLine("\nTell me the name of the product you want to buy. If you are done shopping type 'Done'");
            string buyItem = Console.ReadLine();

            if(buyItem.ToLower().Equals("done"))
            {
                isDone = true;
                CalculateTotalPrice();
            }

            ShoppingItem tempItem = new ShoppingItem();
            int tempAmount = 0;
            bool didFindItem = false;

            foreach(var item in Storage)
            {
                if (item.Key.name.Equals(buyItem)) //Kollar om itemet (namnet) finns i butiken
                {
                    if (ShoppingCart.ContainsKey(item.Key)) //Kollar om personen redan har köpt produkten och lägger till ett exemplar
                    {
                        int amount = ShoppingCart[item.Key];
                        amount++;
                        ShoppingCart[item.Key] = amount;
                    }
                    else
                    {
                        ShoppingCart.Add(item.Key, 1);
                    }

                    int stockCount = item.Value;
                    stockCount--;

                    tempItem = item.Key;
                    tempAmount = stockCount;

                    didFindItem = true;
                }
            }

            if(didFindItem)
            {
                if (tempAmount != 0)
                {
                    Storage[tempItem] = tempAmount;
                }
                else
                {
                    Storage.Remove(tempItem);
                }
            }

        }

        private void ListCart()
        {
            Console.WriteLine("\nYour current shopping cart:");
            foreach(var item in ShoppingCart)
            {
                float totalPrice = item.Value * item.Key.price;
                Console.WriteLine(item.Key.name + " || " + item.Key.price + "kr" + " || " + item.Value + "st" + " || " + "Utgångsdatum:" + item.Key.expireDate + " || " + "Totalpris: " + totalPrice + "kr");
            }
        }

        private void CalculateTotalPrice()
        {
            float totalSum = 0;
            foreach(var item in ShoppingCart)
            {
                float itemsPrice = item.Key.price * item.Value;
                if (item.Value > 5)
                    itemsPrice *= 0.9f;
                totalSum += itemsPrice;
            }

            if (totalSum < 400 || ShoppingCart.Count <= 5)
                totalSum += 50;

            if (isReturningCustomer)
                totalSum *= 0.9f;
            
            Console.WriteLine("Total cost: " + totalSum + "kr");
            if (isDone)
                Environment.Exit(0);
        }

        private void FillStorage()
        {
            ShoppingItem item = new ShoppingItem("Kyckling", 19.90f, Convert.ToDateTime("2017-11-01"));
            Storage.Add(item, 2);
            item = new ShoppingItem("Potatis", 5.30f, Convert.ToDateTime("2018-04-23"));
            Storage.Add(item, 1000);
        }
    }
}
