﻿using System;
namespace CodeNight_20171101
{
    public class ShoppingItem //TODO: Gör singleton. Kan då skapa lagret vid skapandet av klassen
    {
        public string name { get; set; }
        //public int stockCount { get; set; }
        public float price { get; set; }
        public DateTime expireDate { get; set; } //Gemensam för alla inom samma produkt
        public ShoppingItem(string name, float price, DateTime expireDate)
        {
            this.name = name;
            //this.stockCount = stockCount;
            this.price = price;
            this.expireDate = expireDate;
        }
        public ShoppingItem()
        {
            
        }
    }
}
